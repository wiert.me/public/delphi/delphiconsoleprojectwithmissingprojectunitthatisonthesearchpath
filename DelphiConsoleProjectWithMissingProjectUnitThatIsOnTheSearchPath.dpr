program DelphiConsoleProjectWithMissingProjectUnitThatIsOnTheSearchPath;

{$APPTYPE CONSOLE}

uses
  UsedUnit in 'UnitDirectory\UsedUnit.pas';

begin
// The above uses should cause a "[dcc32 Fatal Error] DelphiConsoleProjectWithMissingProjectUnitThatIsOnTheSearchPath.dpr(9): F2613 Unit 'UnitDirectory\UsedUnit.pas' not found."
// Delphi 10.2 Tokyo and up fail to do this when the unit is in the project search path.

// Ctrl-Enter on the unit in Delphi 10.2 Tokyo and up gives you this, despite the project compiling
(*
---------------------------
Error
---------------------------
Cannot open file "D:\Versioned\gitlab.com\wiert.me\public\delphi\delphiconsoleprojectwithmissingprojectunitthatisonthesearchpath\UnitDirectory\UsedUnit.pas". Het systeem kan het opgegeven bestand niet vinden.
---------------------------
OK
---------------------------
*)
// Hover over the unit name in Delphi 10.2 Tokyo and up fails. But the code compiles.
  Writeln(One);
end.
