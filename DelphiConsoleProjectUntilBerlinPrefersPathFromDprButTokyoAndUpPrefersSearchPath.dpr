program DelphiConsoleProjectUntilBerlinPrefersPathFromDprButTokyoAndUpPrefersSearchPath;

{$APPTYPE CONSOLE}

uses
  ConstsUnit in 'UnitDirectory\ConstsUnit.pas';

begin
  // Compiled with Delphi 1-10.1 Berlin will write the value from the unit in the project.
  // Compiled with Delphi 10.2 Tokyo and up will write a different value: the one from the unit in the search path.
  // The IDE of Delphi 10.2 Tokyo and up will still show the value from the unit in the project when:
  // - hovering with the mouse
  // - ctrl-clicking
  Writeln(GlobalConst); // hovering and ctrl-click behaviour depends:
  // in Delphi 2005-10.1 Berlin,
  // - hovering always shows the value in the unit file as specified in the .dpr
  // - ctrl-click always goes to the value inside the unit file specified in the .dpr
  // in Delphi 10.2 Tokyo and up, sometimes you see the 2005-10.1 Berlin behaviour, but often:
  // - hovering shows the value in the unit file on the search path (but does not mention the file it found)
  // - ctrl-click goes to the value inside the unit on the unit search path (file save-as then allows you to see the exact file location)

  // The only workaround is to ensure that the units in your .dpr file are all un the search path
  // before any other directory in the search path.
end.
